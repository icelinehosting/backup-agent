package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rivo/tview"
	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"iceline.host/backup-agent/internal/agent"
	"iceline.host/backup-agent/internal/termui"
)

const refreshInterval = 500 * time.Millisecond

var runReport bool
var reportPath string
var hold bool

func init () {
	runCmd.PersistentFlags().BoolVar(&runReport, "report", false, "generated a backup report when finished")
	runCmd.PersistentFlags().StringVar(&reportPath, "report-path", "backup-report.json", "filename for the backup report")
	runCmd.PersistentFlags().BoolVar(&hold, "hold", false, "leaves the ui report open until manually terminated")
}

var runCmd = &cobra.Command{
	Use:   "run <operation>",
	Short: "Runs the specified backup operation.",
	Args:  cobra.ExactValidArgs(1),
	Run:   func(cmd *cobra.Command, args []string) {
		logger, loggerLevel, err := makeLogger()
		defer logger.Sync()

		// Check for logger errors
		if err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "failed to create logger:", err)
			os.Exit(1)
		}

		// Make the agent from the config
		a, err := makeAgent(logger)
		if err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "error creating agent:", err)
			os.Exit(1)
		}

		// Initialize the agent
		if err := a.Initialize(cmd.Context()); err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "error initializing agent:", err)
			os.Exit(1)
		}

		// Attempt to retrieve the operation
		operation, exists := a.Operations()[args[0]]
		if !exists {
			_, _ = fmt.Fprintln(os.Stderr, "operation not found:", args[0])
			os.Exit(1)
		}

		// Setup the UI is enabled
		if enableUI {
			// Disable all but fatal log events
			loggerLevel.SetLevel(zapcore.FatalLevel)
		}

		onBackup := operation.OnBackupCreated()

		go func (){
			// Run the operation
			logger.Info("running operation")
			if err := operation.Run(cmd.Context()); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "error running operation:", err)
				os.Exit(1)
			}
			logger.Info("finished running operation")
		}()

		// Wait for the backup to start
		backup := <- onBackup
		onStatus := backup.OnStatusChanged()

		// Watch for backup changes
		completed := make(chan struct{})
		go func () {
			for {
				select {
				case status := <- onStatus:
					if status == agent.BackupCompleted {
						if !hold {
							close(completed)
						}
						if runReport {
							report := struct{
								Backup *agent.Backup `json:"backup"`
								Objects []*agent.BackupObject `json:"objects"`
								Took time.Duration `json:"took"`
							}{
								Backup: backup,
								Objects: backup.Objects(),
								Took: backup.FinishedAt().Sub(backup.StartedAt()),
							}

							b, err := json.Marshal(report)
							if err != nil {
								logger.Error("error generating report",
									zap.Error(err))
							}

							if err := os.WriteFile(reportPath, b, os.ModePerm); err != nil {
								logger.Error("error saving report",
									zap.Error(err))
							}
						}
					}
				}
			}
		}()

		// Start the backup UI if enabled
		var app *tview.Application
		var backupUi *termui.Backup
		if enableUI {
			backupUi = termui.NewBackupUI(backup)
			app = tview.NewApplication().
				SetRoot(backupUi, true).
				SetFocus(backupUi)
		}

		// Setup a ctrl+c stop channel
		c := make(chan os.Signal)
		signal.Notify(c, os.Interrupt, syscall.SIGTERM)

		if backupUi != nil {
			// Start a goroutine to periodically update the UI
			go func (){
				renderTicker := time.NewTicker(time.Millisecond * 100)
			waitLoop:
				for {
					select {
					case <- renderTicker.C:
						// Periodically update the UI with current values
						go app.QueueUpdateDraw(func() {
							backupUi.Update()
						})
					case <-c:
						fmt.Println("received ctrl+c")
						break waitLoop
					case <-completed:
						fmt.Println("backup finished")
						break waitLoop
					}
				}

				fmt.Println("stopping")
				app.Stop()
			}()

			if err := app.Run(); err != nil {
				_, _ = fmt.Fprintln(os.Stderr, "error running ui:", err)
				os.Exit(1)
			}
		} else {
			select {
			case <- c:
				fmt.Println("received ctrl+c")
			case <- completed:
				fmt.Println("backup finished")
			}
		}

		fmt.Println("done")
	},
}
