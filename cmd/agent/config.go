package main

import (
	"fmt"
	"os"

	"github.com/caarlos0/env/v6"
	"github.com/creasty/defaults"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/joho/godotenv"

	"iceline.host/backup-agent/internal/agent"
)

// loadConfig loads and initializes the agent config.
func loadConfig (configPath string) (*agent.Config, error) {
	// Create a blank config
	config := agent.NewConfig()

	// Set the defaults in the config
	if err := defaults.Set(config); err != nil {
		return nil, fmt.Errorf("failed to set config defaults: %w", err)
	}

	// Decode the config
	if err := hclsimple.DecodeFile(configPath, nil, config); err != nil {
		return nil, fmt.Errorf("failed to load configuration: %w", err)
	}

	// Ensure blank fields in parse blocks have default set
	if err := defaults.Set(config); err != nil {
		return nil, fmt.Errorf("failed to set config default fields: %w", err)
	}

	// Load the .env file into the environment
	if err := godotenv.Load(); err != nil && !os.IsNotExist(err) {
		return nil, fmt.Errorf("failed to read .env: %w", err)
	}

	// Parse the environment into the config.
	//
	// We do this after loading config because
	// any set environment variables should
	// override config options.
	if err := env.Parse(config); err != nil {
		return  nil, fmt.Errorf("failed to parse env to config: %w", err)
	}

	return config, nil
}
