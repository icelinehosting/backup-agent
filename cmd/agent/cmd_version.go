package main

import (
	"fmt"

	"github.com/spf13/cobra"

	backup_agent "iceline.host/backup-agent"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Prints the version of the backup agent.",
	Run:   func(cmd *cobra.Command, args []string) {
		fmt.Println("git commit:", backup_agent.GitCommit)
	},
}
