package main

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/fsnotify/fsnotify"
	"github.com/go-playground/validator"
	"github.com/spf13/cobra"
	"github.com/zclconf/go-cty/cty"
	"go.uber.org/zap"

	"iceline.host/backup-agent/internal/agent"
	"iceline.host/backup-agent/internal/storage"
	"iceline.host/backup-agent/internal/storage/b2"
)

func makeAgent(logger *zap.Logger) (*agent.Agent, error) {
	// Load and initialize the config
	config, err := loadConfig(configLocation)
	if err != nil {
		return nil, fmt.Errorf("failed to load config: %w", err)
	}

	// Check the hostname cli flag
	if hostname != "" {
		config.Agent.Hostname = hostname
	}

	// Parse any storage options from the env
	parseStorageOptionsFromEnv(config, logger)

	// Validate the config
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, fmt.Errorf("invalid config: %w", err)
	}

	// Create the agent
	a := createAgent(config, logger)

	return a, nil
}

func run(
	logger *zap.Logger,
	cmd *cobra.Command,
) (restart bool, err error) {
	defer logger.Sync()

	// Setup a file watcher for the config file
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return true, fmt.Errorf("failed to create config watcher: %w", err)
	}
	defer watcher.Close()

	// Add the config file to the watcher
	err = watcher.Add(configLocation)
	if err != nil {
		return true, fmt.Errorf("failed to add config to watcher: %w", err)
	}

	// Make the agent from the config
	a, err := makeAgent(logger)
	if err != nil {
		return true, fmt.Errorf("error creating agent: %w", err)
	}

	// Initialize the agent
	if err := a.Initialize(cmd.Context()); err != nil {
		return true, fmt.Errorf("error initializing agent: %w", err)
	}

	// Setup a ctrl+c stop channel
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	errc := make(chan error)

	// Run the agent in a goroutine
	go func() {
		if err := a.Run(cmd.Context()); err != nil {
			errc <- err
		}
	}()

	stop := false

watchLoop:
	for {
		// Wait for a shutdown condition
		select {
		case err := <-errc:
			logger.Info("fatal error running agent",
				zap.Error(err))

			break watchLoop
		case <-c:
			stop = true
			logger.Info("received stop signal")
			break watchLoop
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}

			if event.Op&fsnotify.Write == fsnotify.Write {
				logger.Info("detected config update")
				logger.Info("restarting agent")

				break watchLoop
			}
		case e, ok := <-watcher.Errors:
			if !ok {
				return
			}

			logger.Error("fatal error watching config file",
				zap.Error(e))

			break watchLoop
		}
	}

	// Shutdown the agent
	if err := a.Shutdown(cmd.Context()); err != nil {
		return true, fmt.Errorf("error shutting down agent: %w", err)
	}

	return !stop, nil
}

func parseStorageOptionsFromEnv(config *agent.Config, logger *zap.Logger) {
	for _, pair := range os.Environ() {
		if strings.HasPrefix(pair, "STORAGE_OPTION_") {
			parts := strings.Split(pair, "=")
			name := strings.ToLower(strings.TrimPrefix(parts[0], "STORAGE_OPTION_"))
			config.Agent.Storage.Options[name] = cty.StringVal(parts[1])
			logger.Info("loading storage option from env",
				zap.String("option", name))
		}
	}
}

func createAgent(config *agent.Config, logger *zap.Logger) *agent.Agent {
	agentLogger := logger.With(zap.String("agent", config.Agent.Hostname))

	agentConfig := *config.Agent

	agentDrivers := []*storage.Driver{
		b2.Driver,
	}

	return agent.New(agentLogger, agentConfig, agentDrivers)
}
