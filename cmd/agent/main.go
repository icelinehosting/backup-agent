package main

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"
	"go.uber.org/zap"

	"iceline.host/backup-agent/internal/agent"
)

var (
	configLocation string
	debug          bool
	hostname       string
	enableUI       bool
)

func init() {
	rootCmd.PersistentFlags().StringVarP(&configLocation, "config", "C", agent.DefaultConfigLocation, "location of the agent config")
	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "enables debug logging")
	rootCmd.PersistentFlags().BoolVar(&enableUI, "ui", false, "enables the user-friendly terminal ui")

	hn, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	rootCmd.PersistentFlags().StringVarP(&hostname, "hostname", "H", hn, "specifies the agent's hostname")

	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(runCmd)
}

func makeLogger() (*zap.Logger, zap.AtomicLevel, error) {
	var config zap.Config

	config = zap.NewProductionConfig()

	// Create the dev logger in case if debug mode is enabled
	if debug {
		config = zap.NewDevelopmentConfig()
	}

	atom := zap.NewAtomicLevel()
	config.Level = atom

	logger, err := config.Build()
	if err != nil {
		return nil, atom, err
	}

	return logger, atom, err
}

var rootCmd = &cobra.Command{
	Use:   "backup-agent",
	Short: "Central API daemon for managing backups.",
	Run: func(cmd *cobra.Command, args []string) {
		logger, _, err := makeLogger()
		// Check for logger errors
		if err != nil {
			_, _ = fmt.Fprintln(os.Stderr, "failed to create logger:", err)
			os.Exit(1)
		}

		// Run the agent in a restart loop
		for {
			restart, err := run(logger, cmd)
			if err != nil {
				logger.Error("fatal error running agent", zap.Error(err))
			}

			if !restart {
				break
			}

			logger.Info("restarting in 5 seconds")
			<-time.After(time.Second * 5)
		}
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
