#!/bin/bash

AGENT_PATH="/usr/bin/backup-agent"

# Determine the download URL based on the system architecture
ARCH=$(dpkg --print-architecture)
if [[ $ARCH = "arm" ]]
then
  echo "Using ARM build"
  AGENT_URL="https://gitlab.com/icelinehosting/backup-agent/-/jobs/artifacts/master/raw/agent_linux_arm?job=build_linux_arm"
elif [[ $ARCH = "amd64" ]]
then
  echo "Using AMD64 build"
  AGENT_URL="https://gitlab.com/icelinehosting/backup-agent/-/jobs/artifacts/master/raw/agent_linux_amd64?job=build_linux_amd64"
fi

# Remove the old agent if it exists.
if [ -f "$AGENT_PATH" ]; then
  rm -rf $AGENT_PATH
fi

# Fetch the agent executable.
#
# NOTE: We don't check if it already exists as
# we assume if the script is ran a second time
# we want to upgrade the agent.
echo "Downloading agent executable to $AGENT_PATH"
wget --no-cookies --no-check-certificate $AGENT_URL -O $AGENT_PATH
chmod +x $AGENT_PATH

DEFAULT_AGENT_CONFIG="https://gitlab.com/icelinehosting/backup-agent/-/raw/master/configs/agent.hcl?inline=false"
AGENT_CONFIG_PATH="/etc/backups/agent.hcl"

# Make the config directory if required
mkdir -p $(dirname $AGENT_CONFIG_PATH)

# Determine if we need to fetch the default agent config
if [ -f "$AGENT_CONFIG_PATH" ]; then
   echo "Config file $AGENT_CONFIG_PATH already exists, skipping "
else
  # Fetch the default config
  echo "Downloading default agent config to $AGENT_CONFIG_PATH"
  wget --no-cookies --no-check-certificate $DEFAULT_AGENT_CONFIG -O $AGENT_CONFIG_PATH

  echo "Ensure you edit the config to add your values"
  echo "Config file location: $AGENT_CONFIG_PATH"
fi

DEFAULT_AGENT_SERVICE="https://gitlab.com/icelinehosting/backup-agent/-/raw/master/init/backup-agent.service?inline=false"
AGENT_SERVICE_PATH="/etc/systemd/system/backup-agent.service"

# Determine if we need to fetch the default agent service
if [ -f "$AGENT_SERVICE_PATH" ]; then
   echo "Service file $AGENT_SERVICE_PATH already exists, skipping "
else
  # Fetch the default service
  echo "Downloading default agent service to $AGENT_SERVICE_PATH"
  wget --no-cookies --no-check-certificate $DEFAULT_AGENT_SERVICE -O $AGENT_SERVICE_PATH
fi

# Reload, enable and start the agent service
systemctl daemon-reload
systemctl enable backup-agent
systemctl start backup-agent
