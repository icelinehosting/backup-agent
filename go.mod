module iceline.host/backup-agent

go 1.16

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/creasty/defaults v1.5.1
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gdamore/tcell/v2 v2.3.11 // indirect
	github.com/gizak/termui/v3 v3.1.0
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/google/gops v0.3.19 // indirect
	github.com/hashicorp/hcl/v2 v2.10.0
	github.com/imdario/mergo v0.3.12
	github.com/joho/godotenv v1.3.0
	github.com/kurin/blazer v0.5.3
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/rivo/tview v0.0.0-20210624165335-29d673af0ce2
	github.com/robfig/cron/v3 v3.0.1
	github.com/rs/xid v1.3.0
	github.com/spf13/afero v1.6.0
	github.com/spf13/cobra v1.2.1
	github.com/yargevad/filepathx v1.0.0
	github.com/zclconf/go-cty v1.8.0
	go.uber.org/atomic v1.7.0
	go.uber.org/zap v1.18.1
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
