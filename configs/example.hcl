# Defines the config for the agent.
agent {
  # Configures the cache for agent backups.
  cache {
    # Specifies the path to the cache directory.
    path = "/var/lib/backups/cache"
  }

  # Configures the remote backup storage for the agent.
  storage {
    # Specifies the driver for remote storage.
    driver = "b2"

    # Specifies the format to use to create backup keys.
    key_format = "${operation.id}/${agent.hostname}/${now.day}-${now.month}-${now.year}/${now.hour}-${now.minute}"

    # Configures the remote storage driver.
    options = {
      id = "my_application_id"
      key = "my_application_key"
      bucket = "my-bucket"
    }
  }

  # Defines a backup operation that backs up the panel
  # cache and logs files in the storage directory.
  operation "panel-backup" {

    # Specifies how many paths within a
    # backup can be processed at once.
    #
    # Reducing this number will reduce the
    # CPU usage as this defines how many
    # archives are being compressed in
    # parallel.
    #processing_threads = 15

    # BufferedFiles specifies how many files are pre-opened
    # and buffered for the compression queue.
    #buffered_files = 15

    paths = [
      # Backup the entire storage directory
      "/var/www/pterodactyl/storage/"
    ]
  }

  operation "file-backups" {
    paths = [
      "/srv/daemon-data/"
    ]
  }

  # Defines a backup operation that backs
  # up the pterodactyl volumes.
  operation "wings-backup" {
    paths = [
      # Backup the individual directories/files inside the
      # volumes directory as individual backup archives.
      "/var/lib/pterodactyl/volumes/*"
    ]

    # Defines files to exclude within the matched operation object paths.
    // TODO: Should this be a paths exclusion list and have
    //   a separate one for object paths?
    exclude = [
      # Large regenerated FiveM files
      "cache", // fivem cache files, mostly contains copies of resource files - regenerated on start
      "alpine", // linux libraries for fivem, generated on update/reinstall

      # Source Engine and steamcmd files
      "steamapps", // cache directory for steam downloaded apps, regenerated on run
      "steam", //
      "Steam", // steam server data, regenerated on start
      "steam_cache", // cache for steam, should be find to ignore?
      "steamcmd", // contains the steamcmd executable, regenerated on update/reinstall
      ".steam", // steam client libraries, regenerated on reinstall

      # Garrys Mod files
      "garrysmod/cache", // cached files that are regenerated on restart

      # Rust files
      "RustDedicated_Data", // dedicated rust data, regenerated on update/reinstall
      "Bundles", // rust bundle data, regenerated on update/reinstall

      # Misc files
      "*.tar.gz", // tar.gz archives aren't normally present, if they are it's probably our own backups
      ".git", // ignore git roots as they will possibly contain large object copies of resources
      "*/.git",
    ]

    # We can override storage settings on a per-operation basis.
    storage {
      options = {
        # Example of specifying a different bucket for this operation.
        bucket = "wings-bucket"
      }
    }
  }

  # Defines a schedule that runs backup operations every night at 12pm.
  schedule "daily-backup" {
    # The cron expression that defines
    # when to run the backup.
    cron = "0 0 * * *"

    # The operations to run on the schedule.
    operations = [
      "panel-backup",
      "wings-backup",
      "file-backups"
    ]
  }

  # Defines a schedule that runs backup operations every minute.
  schedule "minutely-game-backup" {
    # The cron expression that defines
    # when to run the backup.
    cron = "* * * * *"

    # The operations to run on the schedule.
    operations = [
      "panel-backup",
      "wings-backup"
    ]
  }
}
