# Defines the config for the agent.
agent {
  # Configures the cache for agent backups.
  cache {
    # Specifies the path to the cache directory.
    path = "/var/lib/backups/cache"
  }

  # Configures the remote backup storage for the agent.
  storage {
    # Specifies the driver for remote storage.
    driver = "b2"

    # Specifies the format to use to create backup keys.
    key_format = "${operation.id}/${agent.hostname}/${now.day}-${now.month}-${now.year}/${now.hour}-${now.minute}"

    # Configures the remote storage driver.
    options = {
      id = "TODO: SET APPLICATION ID"
      key = "TODO: SET APPLICATION KEY"
      bucket = "TODO: set bucket"
    }
  }

  # Defines a backup operation that backs up various files.
  /* operation "file-backup" {
    # Specifies how many paths within a
    # backup can be processed at once.
    #
    # Reducing this number will reduce the
    # CPU usage as this defines how many
    # archives are being compressed in
    # parallel.
    #processing_threads = 15

    # BufferedFiles specifies how many files are pre-opened
    # and buffered for the compression queue.
    #buffered_files = 15

    paths = [
      # Backup the entire etc directory.
      #"/etc/"

      # Backup the root files within the directory as separate archives.
      #"/etc/*"
    ]
  } */

  # Defines a schedule that regularly runs backup operations.
  /* schedule "daily-file-backup" {
    # The cron expression that defines
    # when to run the backup.
    cron = "0 0 * * *"

    # The operations to run on the schedule.
    operations = [
      "file-backup"
    ]
  } */
}
