# Iceline Backup Agent

This repository contains the server agent for performing compressed file and directory backups to remote storage endpoints. The agent has an internal cron-based scheduler that runs backup "operations" which scan files specifies in a list of backup paths and determines what to compress and upload. 

At this moment, only `tar.gz` archives are created and only uploading to Backblaze's B2 API is supported. However, this functionality is implemented with interface and can be easily extended (see `internal/storage/b2.Uploader` and `internal/agent/backup_object.go#Compress` for implementation details).

# Setup

## Installing

The script at `/scripts/install.sh` will download and setup the backup agent's executable, config and service. You can download and run it in one command using wget.

```bash
wget -O - https://gitlab.com/icelinehosting/backup-agent/-/raw/master/scripts/install.sh?inline=false | bash
```

## Configuring

After installing, you'll need to edit the config at `/etc/backups/agent.hcl` to populate the storage driver's authorization keys. You'll also want to edit it to add your backup operations, and to add any schedules for automated backups.

You can find an advanced example of a configuration file for the agent at `/configs/example.hcl`.

The backup agent will automatically restart when the configuration file is updated, you can verify this by checking the agent logs `systemctl status backup-agent`.

# Upgrading

To upgrade the daemon, just run the installation script again and it will replace the existing backup agent binary and restart the systemd job.

# Commands

## `backup-agent`

Runs the backup agent daemon that manages running schedules using an internal cron table based on [github.com/robfig/cron](https://github.com/robfig/cron). This will typically be ran from a systemd service, so it can run in the background and be automatically on reboots. 

### Flags

| Name         | Description
| ---          | ---
| --config, -C | Specifies the config file to use.

## `backup-agent run <operation>`

Runs the specified operation.

> This will be run in a newly created agent instance spawned by the command, use with caution as there is a potential for it to interfere with file access with a demonized agent running schedules.
> 
>

### Flags

| Name     | Description
| ---      | ---
| --ui     | Enables a terminal UI that shows the backup progress in detail.
| --hold   | Prevents the UI from closing when the backup finished, useful for debugging and checking if a backup finished as expected.
| --report | Generates a `backup-report.json` file in the local directory when the backup finished that contains stats about the backup.