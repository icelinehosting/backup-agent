package agent

import "io"

var _ io.Writer = (*ProgressWriter)(nil)

// ProgressWriter updated the object status on writing.
type ProgressWriter struct {
	w io.Writer
	o *BackupObject
}

func (w *ProgressWriter) Write(p []byte) (n int, err error) {
	w.o.UploadState.Store("writing")
	n, err = w.w.Write(p)
	w.o.UploadState.Store("written")

	return n, err
}

// NewProgressWriter makes a new Writer that updates the object status.
func NewProgressWriter(w io.Writer, o *BackupObject) *ProgressWriter {
	return &ProgressWriter{
		w: w,
		o: o,
	}
}
