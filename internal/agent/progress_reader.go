package agent

import (
	"io"

	"go.uber.org/atomic"
)

// ProgressReader counts the bytes read through it.
type ProgressReader struct {
	r io.Reader
	o *BackupObject
	n atomic.Int64
}

// NewReader makes a new Reader that counts the bytes read through it.
func NewProgressReader(r io.Reader, o *BackupObject) *ProgressReader {
	return &ProgressReader{
		r: r,
		o: o,
	}
}

func (r *ProgressReader) Read(p []byte) (n int, err error) {
	r.o.UploadState.Store("reading")
	n, err = r.r.Read(p)
	r.o.UploadState.Store("read")

	r.o.BytesUploaded.Add(int64(n))

	return
}

// N gets the number of bytes that have been read
// so far.
func (r *ProgressReader) N() int64 {
	return r.n.Load()
}
