package agent

// BackupObjectStatus indicates the Status of a backup object.
type BackupObjectStatus string

var (
	BackupObjectPending     BackupObjectStatus = "pending"
	BackupObjectCompressing BackupObjectStatus = "compressing"
	BackupObjectCompressed  BackupObjectStatus = "compressed"
	BackupObjectUploading   BackupObjectStatus = "uploading"
	BackupObjectUploaded    BackupObjectStatus = "uploaded"
	BackupObjectError       BackupObjectStatus = "error"
	BackupObjectCleaning    BackupObjectStatus = "cleaning"
	BackupObjectCompleted   BackupObjectStatus = "completed"
)
