package agent

import (
	"context"
	"fmt"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/rs/xid"
	"github.com/spf13/afero"
	"github.com/yargevad/filepathx"
	"go.uber.org/zap"

	"iceline.host/backup-agent/internal/storage"
)

// newBackup creates a new backup.
func newBackup(
	logger *zap.Logger,
	conf OperationConfig,
	id xid.ID,
	paths []string,
	cache afero.Fs,
	uploader storage.Uploader,
) *Backup {
	return &Backup{
		logger: logger,
		conf:   conf,

		ID:    id,
		Paths: paths,

		cache:    cache,
		uploader: uploader,

		createdAt: time.Now(),

		status: BackupPending,
	}
}

// Backup provides operations for a backup.
type Backup struct {
	logger *zap.Logger
	conf   OperationConfig

	// ID provides a unique backup ID.
	ID xid.ID

	// paths specifies the Path patterns to include the in backup.
	Paths []string
	// MatchedPaths contains the paths the pattern paths matched.
	MatchedPaths []string

	// backupCache provides the filesystem for caching
	// generated backup files before upload.
	cache afero.Fs

	// uploader provides the storage uploader
	// for uploading the backup's objects.
	uploader storage.Uploader

	// createdAt indicates when the backup was initially created.
	createdAt time.Time

	// StartedAt indicates when the backup started processing.
	startedAtMutex sync.RWMutex
	startedAt      time.Time
	// FinishedAt indicates when the backup finished processing.
	finishedAtMutex sync.RWMutex
	finishedAt      time.Time

	// Status indicates the Status of the backup.
	statusMutex         sync.RWMutex
	status              BackupStatus
	statusListenerMutex sync.RWMutex
	statusListeners     []chan BackupStatus

	// objects contains the scanned objects from the backup.
	objects                []*BackupObject
	objectCreatedMutex     sync.RWMutex
	objectCreatedListeners []chan *BackupObject
}

func (b *Backup) Status() BackupStatus {
	b.statusMutex.RLock()
	defer b.statusMutex.RUnlock()
	return b.status
}

func (b *Backup) StartedAt() time.Time {
	b.startedAtMutex.RLock()
	defer b.startedAtMutex.RUnlock()
	return b.startedAt
}

func (b *Backup) FinishedAt() time.Time {
	b.finishedAtMutex.RLock()
	defer b.finishedAtMutex.RUnlock()
	return b.finishedAt
}

func (b *Backup) OnStatusChanged() chan BackupStatus {
	b.statusListenerMutex.Lock()
	defer b.statusListenerMutex.Unlock()
	ch := make(chan BackupStatus)
	b.statusListeners = append(b.statusListeners, ch)
	return ch
}

func (b *Backup) OnObjectCreated() chan *BackupObject {
	b.objectCreatedMutex.Lock()
	defer b.objectCreatedMutex.Unlock()
	ch := make(chan *BackupObject)
	b.objectCreatedListeners = append(b.objectCreatedListeners, ch)
	return ch
}

func (b *Backup) updateStatus(status BackupStatus) {
	b.statusMutex.Lock()
	b.status = status
	b.statusMutex.Unlock()

	b.statusListenerMutex.RLock()
	defer b.statusListenerMutex.RUnlock()
	for _, listener := range b.statusListeners {
		listener <- status
	}
}

// Objects retrieves the objects in the backup.
func (b *Backup) Objects() []*BackupObject {
	return b.objects
}

// Scan the backup paths for changes.
func (b *Backup) Scan() error {
	b.updateStatus(BackupScanning)

	// Loop over the paths to determine the backup objects.
	for _, backupPath := range b.Paths {
		// Retrieve the Path before any expansion operators
		rootPath := strings.Split(backupPath, "*")[0]

		// Process any globs in the source Path
		matches, err := filepathx.Glob(backupPath)
		if err != nil {
			return fmt.Errorf("error parsing path glob: %w path=%s", err, backupPath)
		}

		b.MatchedPaths = append(b.MatchedPaths, matches...)

		b.logger.Info("scanning backup Path",
			zap.String("Path", backupPath),
			zap.String("root", rootPath),
			zap.Strings("matches", matches))

		// Generate objects for the matches
		for _, match := range matches {
			// Generate the backup object Key relative to the root directory.
			//
			// To do this, we first clean and normalize the paths to using
			// forward (/) slashes. Then we strip the root Path of the backup
			// from the matched child Path. Then we strip any leading slashed
			// from the result.
			key := strings.TrimPrefix(filepath.ToSlash(match), filepath.ToSlash(path.Clean(rootPath)))
			key = strings.TrimPrefix(key, "/")

			// Log the discovered object
			b.logger.Info("discovered backup object",
				zap.String("Key", key),
				zap.String("Path", match),
				zap.String("pattern", backupPath))

			// Create the object instance
			backupObject := newBackupObject(
				b.logger.With(zap.String("object", key)),
				b.conf,
				match,
				key,

				// Pass the backup cache filesystem to the
				// backup object so it can generate it's
				// backup objects within the cache fs.
				b.cache,
			)
			b.objects = append(b.objects, backupObject)

			// Call the backup object listeners
			b.objectCreatedMutex.RLock()
			for _, listener := range b.objectCreatedListeners {
				listener <- backupObject
			}
			b.objectCreatedMutex.RUnlock()
		}
	}

	return nil
}

func (b *Backup) processWorkerObject(ctx context.Context, object *BackupObject, logger *zap.Logger) {
	// Tries to ensure the object cache is removed when the backup finishes
	defer object.Cleanup()

	// Process the backup object
	logger.Info("processing object",
		zap.String("object", object.Key))
	if err := object.Process(); err != nil {
		b.logger.Error("failed to process object",
			zap.String("object", object.Key),
			zap.Error(err))
		return
	}

	// Upload the object
	b.updateStatus(BackupUploading)
	if err := b.uploadObject(ctx, object); err != nil {
		b.logger.Error("failed to upload object",
			zap.String("object", object.Key),
			zap.Error(err))
		return
	}
}

// processWorker provides a worker for processing backup objects.
func (b *Backup) processWorker(ctx context.Context, ch <-chan *BackupObject, logger *zap.Logger) {
	for object := range ch {
		b.processWorkerObject(ctx, object, logger.With(
			zap.String("object", object.Key)))
	}
}

// Process scanned backup objects.
func (b *Backup) Process(ctx context.Context, workers int) error {
	b.updateStatus(BackupProcessing)

	wg := sync.WaitGroup{}

	objects := make(chan *BackupObject, workers)

	// Launch the object workers.
	//
	// We launch multiple concurrent workers so
	// that multiple backup objects can be
	// processed at the same time for efficiency.
	for i := 0; i < workers; i++ {
		wg.Add(1)
		i := i
		go func(wg *sync.WaitGroup, j int, objs <-chan *BackupObject) {
			b.processWorker(ctx, objs, b.logger.With(
				zap.Int("worker", j)))
			wg.Done()
		}(&wg, i, objects)
	}

	// Add the backup objects into the processing queue.
	//
	// If the queue is full then there are no workers
	// available and the loop will block until one is
	// available.
	for _, object := range b.objects {
		objects <- object
	}

	close(objects)

	// Wait for any remaining jobs to finish processing
	wg.Wait()

	return nil
}

func (b *Backup) uploadObject(ctx context.Context, object *BackupObject) error {
	b.logger.Info("starting object upload")

	// Create a writer to upload the object
	w := b.uploader.Writer(ctx, object.Key)
	defer w.Close()

	// Upload the object.
	//
	// We allow the object to close the writer so we can ensure
	// it's not closed after the uploaded object steam is closed.
	bytesUploaded, err := object.Upload(ctx, w)
	if err != nil {
		b.logger.Error("failed to upload object",
			zap.String("object", object.Key),
			zap.Error(err))

		return fmt.Errorf("error uploading object: %w object=%s", err, object.Key)
	}

	b.logger.Info("finished object upload",
		zap.Int64("bytes", bytesUploaded))

	return nil
}

// Run the backup.
func (b *Backup) Run(ctx context.Context) error {
	b.startedAtMutex.Lock()
	b.startedAt = time.Now()
	b.startedAtMutex.Unlock()

	// Scan the backup paths for objects
	b.logger.Info("starting backup object scanning")
	if err := b.Scan(); err != nil {
		b.logger.Error("failed to scan backup",
			zap.Error(err))

		return fmt.Errorf("error scanning backup: %w", err)
	}

	// Process the backup objects.
	//
	// This will launch several goroutine workers
	// to asynchronously process multiple backup
	// objects at the same time.
	b.logger.Info("starting backup object processing")
	if err := b.Process(ctx, b.conf.ProcessingThreads); err != nil {
		b.logger.Error("failed to process backup",
			zap.Error(err))

		return fmt.Errorf("error processing backup: %w", err)
	}

	b.updateStatus(BackupCompleted)

	b.finishedAtMutex.Lock()
	defer b.finishedAtMutex.Unlock()
	b.finishedAt = time.Now()

	return nil
}
