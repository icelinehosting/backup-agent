package agent

import (
	"context"
	"time"

	"github.com/robfig/cron/v3"
	"go.uber.org/zap"
)

// newSchedule creates a new schedule object.
func newSchedule (
	logger *zap.Logger,
	config *ScheduleConfig,

	operations []*Operation,
) *Schedule {
	return &Schedule{
		logger: logger,
		config: config,

		operations: operations,
	}
}

// Schedule defines an agent schedule.
type Schedule struct {
	logger *zap.Logger
	config *ScheduleConfig

	// operations defines the operations
	// to execute for the schedule.
	operations []*Operation

	// cronId specifies the ID of the
	// schedule's cron table entry.
	cronId cron.EntryID

	// cronTable is a pointer to the agent's
	// cron table for the scheduler.
	cronTable *cron.Cron
}

// Run the schedule operations.
func (s Schedule) Run() {
	s.logger.Info("running schedule")
	start := time.Now()

	// Create a new context for the schedule run.
	//
	// TODO: There should be a deadline attached to
	//  this context so it doesn't run indefinitely.
	ctx := context.Background()

	// Run the operations in the schedule
	for _, operation := range s.operations {
		s.logger.Info("running schedule operation",
			zap.String("operation", operation.config.ID))

		// Run the operation
		if err := operation.Run(ctx); err != nil {
			s.logger.Error("error running operation",
				zap.String("operation", operation.config.ID),
				zap.Error(err))
		}

		s.logger.Info("finished running schedule operation",
			zap.String("operation", operation.config.ID))
	}

	// Get the cron entry so we can check when the next run it
	entry := s.cronTable.Entry(s.cronId)

	s.logger.Info("finished running schedule",
		zap.Duration("took", time.Now().Sub(start)),
		zap.Time("next_run", entry.Next))
}

