package agent

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"github.com/rs/xid"
	"github.com/spf13/afero"
	"github.com/zclconf/go-cty/cty"
	"go.uber.org/zap"

	"iceline.host/backup-agent/internal/storage"
)

// KeyFormatter provides a formatter for operation keys.
type KeyFormatter func(
	agent *Agent,
	operation *Operation,
	backup string,
) (string, error)

// newOperation creates a new operation object.
func newOperation(
	agent *Agent,
	logger *zap.Logger,
	config *OperationConfig,
	agentConfig AgentConfig,
	keyFormatter KeyFormatter,
	storage *storage.Driver,
	storageOptions map[string]cty.Value,
	cache afero.Fs,
) *Operation {
	return &Operation{
		agent: agent,

		logger:      logger,
		config:      config,
		agentConfig: agentConfig,

		keyFormatter: keyFormatter,

		storage:        storage,
		storageOptions: storageOptions,

		cache: cache,

		backups: make(map[string]*Backup),
	}
}

// Operation defines an operation.
type Operation struct {
	agent *Agent

	logger *zap.Logger

	config      *OperationConfig
	agentConfig AgentConfig

	// keyFormatter provides the formatter for object
	// keys for backups created by this operation.
	keyFormatter KeyFormatter

	// storage provides the remote storage driver
	// for uploading operation backups.
	storage        *storage.Driver
	storageOptions map[string]cty.Value

	// cache provides the filesystem for caching
	// backups created by this operation.
	cache afero.Fs

	// backups contains the in-process operation backups.
	//
	// This is governed by a mutex backup the operation
	// run can be called from a different routine each
	// time a schedule is triggered by the cron table.
	backups      map[string]*Backup
	backupsMutex sync.RWMutex

	// backupCreatedListener contains listeners for
	// when a backup is created by an operation.
	backupCreatedListener []chan *Backup
}

func (o *Operation) OnBackupCreated() chan *Backup {
	ch := make(chan *Backup)
	o.backupCreatedListener = append(o.backupCreatedListener, ch)
	return ch
}

func (o *Operation) run(ctx context.Context) error {
	backupId := xid.New()

	// Generate the backup Key
	o.logger.Debug("generating backup Key")
	key, err := o.keyFormatter(o.agent, o, backupId.String())
	if err != nil {
		return fmt.Errorf("failed to create backup Key: %w", err)
	}
	o.logger.Debug("generated backup Key",
		zap.String("Key", key))

	// Create the uploader for the backup
	o.logger.Debug("acquiring backup uploader",
		zap.String("Key", key))
	uploader, err := o.storage.Uploader(ctx, o.storageOptions, key)
	if err != nil {
		return fmt.Errorf("failed to create backup uploader: %w", err)
	}

	// Create the cache for the backup files
	//
	// NOTE: we do not create the directory, as in some cases
	//   such as with a single file upload the backup cache fs
	//   will be an archive and not a directory.
	backupCache := afero.NewBasePathFs(o.cache, backupId.String())

	// Create the operation backup
	backup := newBackup(
		o.logger.With(
			zap.String("backup", backupId.String())),
		*o.config,
		backupId,
		o.config.Paths,
		backupCache,
		uploader)

	// Add the backup to the registry
	o.backupsMutex.Lock()
	o.backups[backupId.String()] = backup
	o.backupsMutex.Unlock()

	// Inform any listeners
	for _, listener := range o.backupCreatedListener {
		listener <- backup
	}

	defer func(b *Backup) {
		o.generateReport(b)

		// Remove any cache files created by the
		// backup now that it's completed.
		if err := backupCache.RemoveAll("/"); err != nil {
			b.logger.Info("failed to remove backup cache", zap.Error(err))
		}

		// Remove the backup from the operation backups list
		delete(o.backups, backupId.String())
	}(backup)

	// Run the backup
	o.logger.Info("running operation backup",
		zap.String("backup", backupId.String()))
	if err := backup.Run(ctx); err != nil {
		return fmt.Errorf("error running backup: %w", err)
	}

	return nil
}

// Run the operation.
func (o *Operation) Run(ctx context.Context) error {
	//o.logger.Info("running operation")

	return o.run(ctx)
}

func (o *Operation) generateReport(b *Backup) {
	// Check if backup reports are enabled
	if o.config.Reports.Enabled {
		path := filepath.Join(
			o.agentConfig.Reports.Path,
			o.config.ID,
			fmt.Sprintf("%s.json", b.ID.String()))

		// Create the reports directory
		if err := os.MkdirAll(filepath.Dir(path), os.ModePerm); err != nil {
			b.logger.Info("failed to create backup report directory",
				zap.String("path", filepath.Base(path)),
				zap.Error(err))
		}

		// Save the backup report
		if err := b.SaveReport(path); err != nil {
			b.logger.Info("failed to save backup report", zap.Error(err))
		}
		b.logger.Info("saved backup report",
			zap.String("path", path))
	}
}
