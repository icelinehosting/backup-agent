package agent

import (
	"github.com/hashicorp/hcl/v2"
	"github.com/zclconf/go-cty/cty"
)

// OperationStorageConfig provides the config overrides
// for remote storage on operations.
type OperationStorageConfig struct {
	// Driver specifies the driver for remote storage.
	Driver string `hcl:"driver,attr"`

	// KeyFormat specifies the format for backup keys.
	KeyFormat hcl.Expression `hcl:"key_format,attr"`

	// Options specifies configuration options for the storage driver.
	Options map[string]cty.Value `hcl:"options,attr"`
}

// OperationReportsConfig provides the config for an operation report.
type OperationReportsConfig struct {
	// Enabled indicates if operation reports are enabled.
	Enabled bool `default:"true" hcl:"enabled,optional"`
}

// OperationConfig provides the config for an operation.
type OperationConfig struct {
	// ID provides a unique ID for the operation.
	ID string `hcl:",label" validate:"required"`

	// Paths defines paths for the operation.
	Paths []string `hcl:"paths,attr" validate:"required"`

	// Exclude defines paths or patterns to exclude
	// from the backup.
	Exclude []string `hcl:"exclude,optional"`

	// ProcessingThreads specifies how many concurrent
	// threads are used for compressing and uploading
	// backup objects.
	//
	// This determines how many backup objects are
	// asynchronously processed at the same time.
	ProcessingThreads int `default:"5" hcl:"processing_threads,optional"`

	// BufferedFiles specifies how many files are pre-opened
	// and buffered for the compression queue.
	//
	// Files are opened and prepared for reading ahead of time
	// because quite often the disk I/O operations of opening the
	// file are the slowest part of the archive compression.
	BufferedFiles int `default:"8" hcl:"buffered_files,optional"`

	// Storage provides the config for storage for the operation.
	//
	// This is merged with the agent storage config to produce
	// to final config used for the backup upload.
	Storage *OperationStorageConfig `hcl:"storage,block"`

	// Reports provides settings for configuring post-operation reports.
	Reports OperationReportsConfig `default:"{\"Enabled\": true}"`
}

// ScheduleConfig provides the config for a scheduler.
type ScheduleConfig struct {
	// ID defines a unique ID for the schedule.
	ID string `hcl:",label" validate:"required"`

	// Cron defines the cron for the schedule.
	Cron string `hcl:"cron,attr" validate:"required"`

	// Operations defines the operations the schedule will run.
	Operations []string `hcl:"operations,attr" validate:"required"`
}

// StorageConfig provides the config for remote storage.
type StorageConfig struct {
	// Driver specifies the driver for remote storage.
	Driver string `env:"STORAGE_DRIVER" default:"b2" hcl:"driver,attr"`

	// KeyFormat specifies the format for backup keys.
	KeyFormat hcl.Expression `hcl:"key_format,attr"`

	// Config specifies configuration options for the storage driver.
	Options map[string]cty.Value `hcl:"options,attr"`
}

// CacheConfig provides the config for the agent cache.
type CacheConfig struct {
	// Path specifies the Path to the cache directory.
	Path string `env:"CACHE_PATH" default:"/var/lib/backups/cache" hcl:"path,attr" validate:"required"`
}

// ReportConfig provides a config for backup reports.
type ReportConfig struct {
	// Path specifies the directory to save reports to
	Path string `env:"REPORTS_PATH" default:"/var/lib/backups/reports/" hcl:"path,optional"`
}

// AgentConfig provides the config for the agent.
type AgentConfig struct {
	// Hostname specifies the hostname of the agent.
	Hostname string `hcl:"hostname,optional"`

	// Cache provides the config for the agent cache.
	Cache *CacheConfig `hcl:"cache,block"`

	// Storage provides the default config for remote backup storage.
	Storage *StorageConfig `hcl:"storage,block"`

	// Reports provides the config for backup reports.
	Reports *ReportConfig `hcl:"reports,block"`

	// Operations provides the config for operations.
	Operations  []*OperationConfig `hcl:"operation,block"`

	// Schedules provides the config for schedules.
	Schedules []*ScheduleConfig `hcl:"schedule,block"`
}

// Config provides the config for the application.
type Config struct {
	// Agent specifies the config for agent.
	Agent  *AgentConfig `hcl:"agent,block"`
}

// NewConfig creates a new default config instance.
func NewConfig() *Config {
	return &Config{
		Agent: &AgentConfig{
			Cache: &CacheConfig{},
			Storage: &StorageConfig{},
			Reports: &ReportConfig{},
		},
	}
}
