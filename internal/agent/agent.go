package agent

import (
	"context"
	"fmt"
	"os"
	"path"
	"time"

	"github.com/hashicorp/hcl/v2"
	"github.com/imdario/mergo"
	"github.com/robfig/cron/v3"
	"github.com/spf13/afero"
	"github.com/zclconf/go-cty/cty"
	"go.uber.org/zap"

	"iceline.host/backup-agent/internal/storage"
)

// New creates a new instance of the backup agent.
func New(logger *zap.Logger, config AgentConfig, drivers []*storage.Driver) *Agent {
	return &Agent{
		logger:  logger,
		config:  config,
		drivers: drivers,
	}
}

// Agent provides the backup management agent.
type Agent struct {
	logger *zap.Logger
	config AgentConfig

	// drivers contains the enabled
	// storage drivers for the agent.
	drivers []*storage.Driver

	// cache provides the cache for files
	// created for agent backups.
	cache afero.Fs

	operations map[string]*Operation
	schedules  []*Schedule

	// initialized indicates if the agent has been initialized
	initialized bool

	// cron contains the cron table for the agent
	cron *cron.Cron
}

// Operations returns the agent operations.
func (a *Agent) Operations() map[string]*Operation {
	return a.operations
}

// Initialize the agent
func (a *Agent) Initialize(ctx context.Context) error {
	a.logger.Info("initializing agent")

	// Initialize the agent's cache filesystem
	if err := a.initializeCache(a.config); err != nil {
		return fmt.Errorf("failed to initialize cache: %w", err)
	}

	// Load the backup operations defined in the config
	if err := a.loadOperations(a.config); err != nil {
		return fmt.Errorf("failed to load operations: %w", err)
	}

	// Load the schedules defined in the agent config
	if err := a.loadSchedules(a.config); err != nil {
		return fmt.Errorf("failed to load schedules: %w", err)
	}

	a.logger.Info("agent initialized")

	// Mark the agent as initialized
	a.initialized = true

	return nil
}

// Run the agent.
func (a *Agent) Run(ctx context.Context) error {
	// Check if the agent was initialized
	if !a.initialized {
		return fmt.Errorf("agent not initialized")
	}

	a.logger.Info("starting agent")

	// Start running the cron table
	a.logger.Info("starting scheduler",
		zap.Int("schedules", len(a.schedules)))
	a.logger.Info("ready")
	a.cron.Run()

	a.logger.Info("agent stopped")
	return nil
}

// Shutdown the agent.
func (a *Agent) Shutdown(ctx context.Context) error {
	a.logger.Info("shutting down agent")

	// Stop the scheduler
	a.logger.Info("stopping scheduler")
	jobCtx := a.cron.Stop()

	a.logger.Info("waiting for running schedules to finish")
	<-jobCtx.Done()

	a.logger.Info("scheduler stopped")

	return nil
}

// initializeCache initializes the agent cache.
func (a *Agent) initializeCache(config AgentConfig) error {
	a.logger.Info("initializing agent cache",
		zap.String("Path", config.Cache.Path))

	// Create the cache directory
	if err := os.MkdirAll(config.Cache.Path, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create cache directory: %w Path=%s", err, config.Cache.Path)
	}

	// Create the cache fs
	cacheFs := afero.NewBasePathFs(afero.NewOsFs(), config.Cache.Path)
	a.cache = cacheFs

	return nil
}

func (a *Agent) loadOperations(config AgentConfig) error {
	a.logger.Info("loading backup operations")

	// Initialize the operations map
	a.operations = make(map[string]*Operation)

	// Load the operations from the config.
	for _, opConfig := range config.Operations {
		// Create the operation cache directory
		cachePath := path.Join("operations", opConfig.ID)
		if err := a.cache.MkdirAll(cachePath, os.ModePerm); err != nil {
			return fmt.Errorf("error creating operation cache: %w Path=%s", err, cachePath)
		}

		// Determine the storage driver name to use
		driverName := a.determinStorageDriverName(opConfig)

		// Create the options for the storage driver
		driverOptions := a.config.Storage.Options
		if opConfig.Storage != nil && opConfig.Storage.Options != nil {
			if err := mergo.Merge(driverOptions, opConfig.Storage.Options, mergo.WithOverride); err != nil {
				return fmt.Errorf("error merging operation storage options with agent storage options: %w", err)
			}
		}
		// Find the storage driver for the operation
		driver, fErr := a.findStorageDriverByDriverName(driverName)
		if fErr != nil {
			return fmt.Errorf("finding storage driver failed: %w", fErr)
		}

		// Check if the driver was found
		if driver == nil {
			return fmt.Errorf("storage driver for operation does not exist: %s", driverName)
		}

		// Create the operation
		op := newOperation(a,
			a.logger.With(zap.String("operation", opConfig.ID)),
			opConfig,
			a.config,
			operationKeyFormatter,
			driver,
			driverOptions,
			afero.NewBasePathFs(a.cache, cachePath))

		// Add the operation to the map
		a.operations[opConfig.ID] = op

		a.logger.Info("loaded backup operation",
			zap.String("operation", opConfig.ID))
	}

	return nil
}

func (a *Agent) loadSchedules(config AgentConfig) error {
	a.logger.Info("loading backup schedules")

	// Initialize the cron table
	a.cron = cron.New()

	// Load the operations from the config
	for _, scheduleConfig := range config.Schedules {
		var operations []*Operation
		var operationNames []string

		// Find the schedule operations
		for _, name := range scheduleConfig.Operations {
			operation, exists := a.operations[name]
			if !exists {
				return fmt.Errorf("operation not found: %s schedule=%s", name, scheduleConfig.ID)
			}

			operations = append(operations, operation)
			operationNames = append(operationNames, name)
		}

		// Create the schedule
		schedule := newSchedule(a.logger.With(
			zap.String("schedule", scheduleConfig.ID)),
			scheduleConfig,
			operations)

		// Add the schedule to the cron table
		entryId, err := a.cron.AddJob(scheduleConfig.Cron, schedule)
		if err != nil {
			return fmt.Errorf("error adding cron entry: %w schedule=%s", err, scheduleConfig.ID)
		}
		schedule.cronId = entryId
		schedule.cronTable = a.cron

		// Add the schedule to the list of agent schedules
		a.schedules = append(a.schedules, schedule)

		a.logger.Info("loaded backup schedule",
			zap.String("schedule", scheduleConfig.ID),
			zap.Strings("operations", operationNames))
	}

	return nil
}

func (a *Agent) determinStorageDriverName(opConfig *OperationConfig) string {
	driverName := a.config.Storage.Driver

	if opConfig.Storage != nil && opConfig.Storage.Driver != "" {
		driverName = opConfig.Storage.Driver
	}

	return driverName
}

func (a *Agent) findStorageDriverByDriverName(driverName string) (*storage.Driver, error) {
	for _, d := range a.drivers {
		if d.ID == driverName {
			return d, nil
		}
	}

	return nil, fmt.Errorf("could not find storage driver by the given driverName: %s", driverName)
}

func operationKeyFormatter(agent *Agent, operation *Operation, backup string) (string, error) {
	now := time.Now()
	id := operation.config.ID
	keyFormat := agent.determineKeyFormat(operation)

	// Build the evaluation context for the Key decoder
	ctx := &hcl.EvalContext{
		Variables: map[string]cty.Value{
			// Pass the agent info to the Key formatter
			"agent": cty.ObjectVal(map[string]cty.Value{
				"hostname": cty.StringVal(agent.config.Hostname),
			}),

			// Pass the operation info to the Key formatter
			"operation": cty.ObjectVal(map[string]cty.Value{
				"id": cty.StringVal(id),
			}),

			// Pass the backup ID to the Key formatter
			"backup": cty.StringVal(backup),

			// Pass the current timestamp to the backup Key formatter
			"now": cty.ObjectVal(map[string]cty.Value{
				"day":    cty.NumberIntVal(int64(now.Day())),
				"month":  cty.NumberIntVal(int64(now.Month())),
				"year":   cty.NumberIntVal(int64(now.Year())),
				"hour":   cty.NumberIntVal(int64(now.Hour())),
				"minute": cty.NumberIntVal(int64(now.Minute())),
				"second": cty.NumberIntVal(int64(now.Second())),
			}),
		},
		Functions: nil,
	}

	// Decode the Key format into a Key
	val, err := keyFormat.Value(ctx)
	if err != nil {
		return "", err
	}
	key := val.AsString()

	return key, nil
}

func (a *Agent) determineKeyFormat(o *Operation) hcl.Expression {
	opConfig := o.config
	keyFormat := a.config.Storage.KeyFormat
	if opConfig.Storage != nil && opConfig.Storage.KeyFormat != nil {
		keyFormat = opConfig.Storage.KeyFormat
	}
	return keyFormat
}
