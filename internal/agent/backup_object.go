package agent

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/afero"
	"github.com/yargevad/filepathx"
	"go.uber.org/atomic"
	"go.uber.org/zap"
)

// newBackupObject creates a new backup object instance.
func newBackupObject(
	logger *zap.Logger,
	conf OperationConfig,
	path string,
	key string,
	cache afero.Fs,
) *BackupObject {
	return &BackupObject{
		logger: logger,
		conf:   conf,
		Path:   path,
		Key:    key,
		cache:  cache,
		Status: BackupObjectPending,
	}
}

// BackupObject defines an object in the backup.
//
// This is typically associated with a singular archive file.
type BackupObject struct {
	logger *zap.Logger
	conf   OperationConfig

	// Path specifies the Path to the directory
	// or file the object is for.
	Path string

	// Key specifies a Key for the backup object,
	// relative to the root of the Path pattern
	// that the object matched.
	Key string

	// cache provides the filesystem for caching
	// generated object files before upload.
	cache afero.Fs

	// Status indicates the Status of the object.
	Status BackupObjectStatus
	// files contains a count of files in the object.
	Files atomic.Int64

	// LastWalked indicates the most recent path walked in the backup object compression.
	LastWalked atomic.String
	// CurrentlyCompressing indicates the mode recently compressed file.
	CurrentlyCompressing atomic.String

	// Size indicates the compressed size of the backup.
	Size atomic.Int64

	// BytesUploaded indicates the progress of the object upload.
	BytesUploaded atomic.Int64
	// UploadState indicates what state the uploader is currently in.
	UploadState atomic.String

	// Error indicates if there is an object error.
	Error atomic.String
}

// updateStatus updates the Status of the backup object.
func (b *BackupObject) updateStatus(status BackupObjectStatus) {
	b.Status = status
}

type compressFile struct {
	path string
	info os.FileInfo
	file *os.File
}

func (b *BackupObject) addFileToArchive(w *tar.Writer, f *compressFile) error {
	b.CurrentlyCompressing.Store(strings.TrimPrefix(f.path, b.Path))

	// Ensure the file is closed
	defer f.file.Close()

	// Trim the prefix and sanitize the output
	filePath := strings.TrimPrefix(filepath.ToSlash(f.path), filepath.ToSlash(b.Path))
	filePath = strings.TrimPrefix(filePath, "/")

	b.logger.Debug("compressing object file",
		zap.String("file", f.path),
		zap.String("info_name", f.info.Name()),
		zap.String("filepath", filePath))

	var link string
	var err error
	if f.info.Mode()&os.ModeSymlink == os.ModeSymlink {
		if link, err = os.Readlink(f.path); err != nil {
			return fmt.Errorf("failed to read link: %w", err)
		}
	}

	// generate tar header
	header, err := tar.FileInfoHeader(f.info, link)
	if err != nil {
		return err
	}

	// must provide real name
	// (see https://golang.org/src/archive/tar/common.go?#L626)
	header.Name = filePath

	// write header
	if err := w.WriteHeader(header); err != nil {
		return err
	}

	// Nothing more to do for non-regular
	if !f.info.Mode().IsRegular() {
		return nil
	}

	// Copy the file into the archive.
	//
	// NOTE: we use a custom buffer over Golang's
	// default io.Copy 32 * 1024 (32KB) buffer for
	// better performance on large files.
	// TODO: Test various buffer sizes
	buf := make([]byte, 32*1024)
	if _, err := io.CopyBuffer(w, f.file, buf); err != nil {
		return err
	}

	b.Files.Add(1)

	return nil
}

// compressAsDirectory compresses the object as if the Path is a directory.
func (b *BackupObject) compressAsDirectory(w *tar.Writer) error {
	// Allow for pre-processing 25 files to compress
	files := make(chan *compressFile, b.conf.BufferedFiles)

	// Build the exclusion list
	var exclude []string
	for _, excludePath := range b.conf.Exclude {
		// Ensure we're operating within the object directory
		excludePath = filepath.Join(b.Path, excludePath)

		// Process any globs in the exclusion path
		matches, err := filepathx.Glob(excludePath)
		if err != nil {
			return fmt.Errorf("error parsing exclution glob: %w exclude=%s", err, excludePath)
		}

		exclude = append(exclude, matches...)
	}

	// Process the files to compress in a concurrent goroutine.
	//
	// The main delay with compressing files is the i/o overhead
	// of opening the files for reading. If we do this in a
	// separate routine we can ensure there are always opened files
	// ready for compression in the the archive.
	go func() {
		// walk through every file in the folder.
		if err := filepath.Walk(b.Path, func(path string, fi os.FileInfo, err error) error {
			if err != nil {
				b.logger.Error("error reading file",
					zap.Error(err))
				return nil
			}

			// Check if the file is excluded
			for _, excludeMatch := range exclude {
				if path == excludeMatch {
					/* b.logger.Info("skipping excluded path",
					zap.String("path", path),
					zap.String("exclude_match", excludeMatch)) */

					// If the path matches the execution, we can assume that
					// the exclusion covers then entire directory contents
					// and safely skip walking the entire directory.
					if fi.IsDir() {
						return filepath.SkipDir
					} else {
						return nil
					}
				}
			}

			b.LastWalked.Store(strings.TrimPrefix(path, b.Path))

			// Open the file for reading
			file, err := os.Open(path)
			if err != nil {
				return err
			}

			files <- &compressFile{
				path: path,
				info: fi,
				file: file,
			}

			return nil
		}); err != nil {
			b.logger.Error("error walking directory",
				zap.Error(err))
		}

		close(files)
	}()

	// Process the queued files for compression
	for f := range files {
		if err := b.addFileToArchive(w, f); err != nil {
			b.logger.Error("error adding file to archive",
				zap.Error(err))
		}
	}

	return nil
}

// compressAsFile compresses the object as if the Path is a file.
func (b *BackupObject) compressAsFile(info os.FileInfo, w *tar.Writer) error {
	// generate tar header
	header, err := tar.FileInfoHeader(info, info.Name())
	if err != nil {
		return err
	}

	// must provide real name
	// (see https://golang.org/src/archive/tar/common.go?#L626)
	header.Name = info.Name()

	// write header
	if err := w.WriteHeader(header); err != nil {
		return err
	}

	// Open the object file
	data, err := os.Open(b.Path)
	if err != nil {
		return err
	}

	// Copy the file to the archive
	if _, err := io.Copy(w, data); err != nil {
		return err
	}

	return nil
}

// Compress the backup object file into a tar.gz archive.
func (b *BackupObject) Compress() error {
	b.updateStatus(BackupObjectCompressing)

	// Stat the object file to check what it is
	info, err := os.Stat(b.Path)
	if err != nil {
		return fmt.Errorf("failed to stat target Path: %w", err)
	}

	// Create the directory for the archive if required.
	//
	// If we're compressing the entire backup Path (i.e.
	// blank Key is equal to /) then we don't want to create
	// the directory as the archive needs to be created there.
	if b.Key != "" {
		b.logger.Info("creating parent cache directory for object",
			zap.String("Path", filepath.Base(b.Key)))
		if strings.Contains(b.Key, "/") {
			if err := b.cache.MkdirAll(filepath.Base(b.Key), os.ModePerm); err != nil {
				return fmt.Errorf("failed to create cache directory: %w", err)
			}
		} else {
			if err := b.cache.MkdirAll("/", os.ModePerm); err != nil {
				return fmt.Errorf("failed to create backupcache directory: %w", err)
			}
		}
	}

	// Create the archive file
	b.logger.Info("creating object archive file",
		zap.String("path", b.Key))
	file, err := b.cache.Create(b.Key)
	if err != nil {
		return fmt.Errorf("failed to create archive file: %w", err)
	}
	defer file.Close()

	// tar > gzip > buf
	zr := gzip.NewWriter(file) // writer
	tw := tar.NewWriter(zr)

	// Check if we're compressing a directory or a single file
	if info.IsDir() {
		b.logger.Debug("beginning compressing of object directory")
		if err := b.compressAsDirectory(tw); err != nil {
			return fmt.Errorf("failed to compress as directory: %w", err)
		}
		b.logger.Debug("finished compression of object directory")
	} else {
		b.logger.Debug("beginning compressing of object file")
		if err := b.compressAsFile(info, tw); err != nil {
			return fmt.Errorf("failed to compress as file: %w", err)
		}
		b.logger.Debug("finished compression of object file")
	}

	b.logger.Debug("finished adding objects to archive")

	// Produce the tar file
	if err := tw.Close(); err != nil {
		return err
	}

	// Produce the gzip file
	if err := zr.Close(); err != nil {
		return err
	}

	b.updateStatus(BackupObjectCompressed)

	b.logger.Debug("finished object compression")

	return nil
}

// Process the backup object to generate the required
// files/archives for uploading the backup.
func (b *BackupObject) Process() error {
	// Compress the object
	if err := b.Compress(); err != nil {
		b.updateStatus(BackupObjectError)
		b.Error.Store("error compressing: " + err.Error())
		return fmt.Errorf("error compressing object: %w", err)
	}

	return nil
}

// Upload the backup object to the specified writer.
func (b *BackupObject) Upload(ctx context.Context, w io.WriteCloser) (int64, error) {
	b.updateStatus(BackupObjectUploading)

	b.UploadState.Store("init")
	b.logger.Info("stating archive for upload")
	stat, err := b.cache.Stat(b.Key)
	if err != nil {
		b.updateStatus(BackupObjectError)
		b.Error.Store("stat error: " + err.Error())
		return -1, fmt.Errorf("failed to stat object file: %w", err)
	}
	b.Size.Store(stat.Size())

	// Open the archive file
	b.UploadState.Store("opening")
	b.logger.Info("opening archive for upload")
	file, err := b.cache.Open(b.Key)
	if err != nil {
		b.updateStatus(BackupObjectError)
		b.Error.Store("open error: " + err.Error())
		return -1, fmt.Errorf("failed to open object file: %w", err)
	}
	defer file.Close()
	defer w.Close()

	// Wrap the file reader in a progress checker.
	// TODO: does this introduce a large overhead?
	uploadReader := NewProgressReader(file, b)

	// Wrap the uploader in a object progress updater.
	uploadWriter := NewProgressWriter(w, b)

	// Upload the object using the passed writer.
	//
	// The calling backup function will handle closing
	// the writer after this function returns.
	b.UploadState.Store("starting")
	b.logger.Info("starting archive upload")
	bytesWritten, cErr := io.Copy(uploadWriter, uploadReader)
	if cErr != nil {
		b.updateStatus(BackupObjectError)
		b.Error.Store("copy error: " + cErr.Error())
		return -1, fmt.Errorf("failed to copy file: %w", cErr)
	}

	// Close the upload stream.
	b.logger.Debug("closing object uploader",
		zap.Int64("bytes", bytesWritten))
	b.UploadState.Store("closing")
	if err := w.Close(); err != nil {
		b.logger.Error("failed to close object uploader",
			zap.Error(err))

		b.updateStatus(BackupObjectError)
		b.Error.Store(err.Error())
		return -1, fmt.Errorf("error closing upload: %w", err)
	}
	b.logger.Info("finished archive upload")
	b.UploadState.Store("completed")

	b.updateStatus(BackupObjectUploaded)

	return bytesWritten, nil
}

// Cleanup and delete any resources
func (b *BackupObject) Cleanup() error {
	b.logger.Info("cleaning up object files")
	b.updateStatus(BackupObjectCleaning)
	if exists, err := afero.Exists(b.cache, b.Key); exists {
		// Delete all the cached files
		if err := b.cache.RemoveAll(b.Key); err != nil {
			b.updateStatus(BackupObjectError)
			b.Error.Store(err.Error())
			return err
		}
	} else if err != nil {
		b.updateStatus(BackupObjectError)
		b.Error.Store(err.Error())
		return err
	}

	b.updateStatus(BackupObjectCompleted)
	return nil
}
