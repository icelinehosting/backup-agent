package agent

// DefaultConfigLocation specifies the default location for the config on Windows.
const DefaultConfigLocation = "./agent.hcl"
