package agent

// BackupStatus indicates the Status of a backup.
type BackupStatus string

var (
	BackupPending    BackupStatus = "pending"
	BackupScanning   BackupStatus = "scanning"
	BackupProcessing BackupStatus = "processing"
	BackupUploading  BackupStatus = "uploading"
	BackupCompleted  BackupStatus = "completed"
)
