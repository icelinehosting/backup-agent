package agent

// DefaultConfigLocation specifies the default location for the config on Linux.
const DefaultConfigLocation = "/etc/backups/agent.hcl"
