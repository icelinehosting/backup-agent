package agent

import (
	"encoding/json"
	"os"
	"path/filepath"
	"time"

	"go.uber.org/zap"
)

// BackupObjectReport provides a report about the backup object.
type BackupObjectReport struct {
	Path   string             `json:"path"`
	Key    string             `json:"key"`
	Status BackupObjectStatus `json:"status"`
	Size   int64              `json:"size"`
	Error  string             `json:"error"`
}

// BackupReport provides a report about a backup.
type BackupReport struct {
	// ID indicates the backup ID.
	ID string `json:"id"`

	// Status indicates
	Status BackupStatus `json:"status"`

	// Paths indicates the paths the backup was created with.
	Paths []string `json:"paths"`
	// MatchedPaths indicates the paths that where matched
	// in the backup path patterns.
	MatchedPaths []string `json:"matchesPaths"`

	// Took indicates how long a backup took to finish.
	Took time.Duration `json:"took"`

	// StartedAt indicates when the backup started.
	StartedAt time.Time `json:"startedAt"`
	// FinishedAt indicates when the backup finished.
	FinishedAt time.Time `json:"finishedAt"`

	// Objects specifies the objects in the backup report
	Objects []BackupObjectReport `json:"objects"`
}

// SaveReport saves a backup report to the specified file.
func (b *Backup) SaveReport(path string) error {
	report := BackupReport{
		ID:           b.ID.String(),
		Status:       b.status,
		Paths:        b.Paths,
		MatchedPaths: b.MatchedPaths,
		StartedAt:    b.startedAt,
	}

	// Add the backup duration and finish time if specified
	if !b.finishedAt.IsZero() {
		report.FinishedAt = b.finishedAt
		report.Took = b.finishedAt.Sub(b.startedAt)
	}

	// Add the backup objects to the report
	for _, object := range b.objects {
		report.Objects = append(report.Objects, BackupObjectReport{
			Path:   object.Path,
			Key:    object.Key,
			Status: object.Status,
			Size:   object.Size.Load(),
			Error:  object.Error.Load(),
		})
	}

	// Encode the backup report to JSON
	bytes, err := json.Marshal(report)
	if err != nil {
		return err
	}

	// Create the reports directory if required
	if err := os.MkdirAll(filepath.Dir(path), os.ModePerm); err != nil {
		b.logger.Info("failed to create backup report directory",
			zap.String("path", filepath.Base(path)),
			zap.Error(err))
		return err
	}

	// Write the report file
	return os.WriteFile(path, bytes, os.ModePerm)
}
