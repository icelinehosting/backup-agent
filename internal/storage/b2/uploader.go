package b2

import (
	"context"
	"fmt"
	"io"
	"path"

	"github.com/kurin/blazer/b2"
	"github.com/zclconf/go-cty/cty"

	"iceline.host/backup-agent/internal/storage"
)

// uploader provides the storage uploader for the b2 driver.
type uploader struct {
	client  *b2.Client
	bucket  *b2.Bucket
	rootKey string
}

// NewUploader creates a new instance of the b2 uploader.
func NewUploader(ctx context.Context, options map[string]cty.Value, key string) (storage.Uploader, error) {
	// Create the b2 client for the uploader
	client, err := b2.NewClient(ctx,
		options["id"].AsString(),
		options["key"].AsString())
	if err != nil {
		return nil, fmt.Errorf("error creating b2 client for uploader: %w", err)
	}

	// Get a handle to the bucket
	bucket, err := client.Bucket(ctx, options["bucket"].AsString())
	if err != nil {
		return nil, fmt.Errorf("error getting handle to b2 bucket for uploader: %w", err)
	}

	return &uploader{
		client:  client,
		bucket:  bucket,
		rootKey: key,
	}, nil
}

func (u uploader) Writer(ctx context.Context, key string) io.WriteCloser {
	fullKey := path.Join(u.rootKey, key)
	obj := u.bucket.Object(fullKey)

	// Create a writer for the object
	w := obj.NewWriter(ctx, b2.WithAttrsOption(&b2.Attrs{
		// TODO: if we allow non-compressed backups then this needs to be adjusted
		ContentType: "application/x-gzip",
	}))

	// Allow for concurrent multipart upload streams
	//w.ConcurrentUploads = 5

	return w
}
