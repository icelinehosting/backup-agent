package b2

import (
	"iceline.host/backup-agent/internal/storage"
)

// Driver provides the storage driver for b2.
var Driver = storage.NewDriver(
	"b2",
	storage.ConfigSchema{
		"id": {
			Name: "Application ID",
			Type: storage.StringOption,
			Description: "The application ID to use for authentication.",
			Required: true,
		},
		"key": {
			Name: "Application Key",
			Type: storage.StringOption,
			Description: "The application key to use for authentication.",
			Required: true,
		},
		"bucket": {
			Name: "Bucket",
			Type: storage.StringOption,
			Description: "The bucket to store the files in.",
			Required: true,
		},
	},
	NewUploader)
