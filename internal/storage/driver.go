package storage

import (
	"context"

	"github.com/zclconf/go-cty/cty"
)

// Driver provides a storage driver.
type Driver struct {
	// ID provides a unique driver ID.
	ID string

	// Config provides the schema
	// for the driver config.
	Config ConfigSchema

	// UploaderFactory provides the factory for creating uploaders.
	UploaderFactory UploaderFactory
}

// NewDriver creates a new driver instance.
func NewDriver(
	id string,
	config ConfigSchema,
	uploaderFactory UploaderFactory,
) *Driver {
	return &Driver{
		ID:              id,
		Config:          config,
		UploaderFactory: uploaderFactory,
	}
}

// ValidateConfig validates the specified driver config.
func (d *Driver) ValidateConfig(v map[string]cty.Value) error {
	return d.Config.Validate(v)
}

// Uploader creates a new uploader for the specified key.
func (d *Driver) Uploader(ctx context.Context, options map[string]cty.Value, key string) (Uploader, error) {
	return d.UploaderFactory(ctx, options, key)
}
