package storage

import (
	"fmt"

	"github.com/go-playground/validator"
	"github.com/zclconf/go-cty/cty"
)

// DriverOption defines an option for a driver.
type DriverOption struct {
	// Name provides a user-friendly name for the option.
	Name string

	// Type indicates the type of the option.
	Type OptionType

	// Description provides a description for the driver options.
	Description string

	// Required indicates if the option is required.
	Required bool

	// Rules provides the validate rules for the option.
	Rules string
}

// ConfigSchema provides the schema for a storage driver.
type ConfigSchema map[string]DriverOption

// Validate the specified config values against the schema.
func (s *ConfigSchema) Validate(v map[string]cty.Value) error {
	validate := validator.New()

	// Validate the passed options
	for parameterName, parameter := range v {
		// Retrieve the validation option from the schema
		option, exists := (*s)[parameterName]
		if !exists {
			return fmt.Errorf("unexpected driver option: %s", parameterName)
		}

		// Check the option value's type
		if typeName := parameter.Type().GoString(); typeName != string(option.Type) {
			return fmt.Errorf("option value is incorrect type: %s expected=%s", typeName, option.Type)
		}

		// Validate the option value using the defined option rules
		if option.Rules != "" {
			if err := validate.Var(parameter, option.Rules); err != nil {
				return fmt.Errorf("option validation failed: %w", err)
			}
		}
	}

	// Check that all required options are set
	for optionName, option := range *s {
		if option.Required {
			if _, exists := v[optionName]; !exists {
				return fmt.Errorf("missing required option: %s", optionName)
			}
		}
	}

	return nil
}

// hasOption checks if the schema has an option with the specified name.
func (s *ConfigSchema) hasOption(name string) bool {
	found := false

	for optionName := range *s {
		if optionName == name {
			found = true
		}
	}

	return found
}
