package storage

type OptionType string

var (
	// String indicates the option is a string.
	StringOption OptionType = "string"

	// Bool indicates the option is a bool.
	BoolOption OptionType = "bool"

	// Int indicates the option is an int.
	IntOption OptionType = "int"
)
