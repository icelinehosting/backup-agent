package storage

import (
	"context"
	"io"

	"github.com/zclconf/go-cty/cty"
)

// Uploader provides the interface for a driver uploader.
type Uploader interface {
	// Writer creates a new writer for the specified key.
	Writer(ctx context.Context, key string) io.WriteCloser
}

// UploaderFactory provides a factory for creating a driver uploader.
type UploaderFactory func(ctx context.Context, options map[string]cty.Value, key string) (Uploader, error)
