package termui

import (
	"fmt"
	"time"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"iceline.host/backup-agent/internal/agent"
)

func NewBackupInfoUI (backup *agent.Backup) *BackupInfo {
	startedAt := tview.NewTextView().SetText("Started At: " + backup.StartedAt().Format(time.Kitchen))
	finishedAt := tview.NewTextView().SetText("Finished At: -")

	took := tview.NewTextView().SetText("Took: ")

	status := tview.NewTextView().SetText("Status: ")

	flex := tview.NewFlex().SetDirection(tview.FlexRow)
	flex.AddItem(tview.NewFlex().SetDirection(tview.FlexColumn).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewTextView().SetText("ID: " + backup.ID.String()), 0, 1, false).
			AddItem(took, 0, 1, false),
			0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(startedAt, 0, 1, false).
			AddItem(finishedAt, 0, 1, false),
			0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(status, 0, 1, false),
			0, 1, false),
		2, 1, false)

	return &BackupInfo{
		Box: tview.NewBox().
			SetBorder(false).
			SetTitle("Backup Info").
			SetBorderPadding(0, 0, 1, 1),

		backup: backup,

		flex: flex,

		startedAt: startedAt,
		finishedAt: finishedAt,
		took: took,
		status: status,
	}
}

var _ tview.Primitive = (*Backup)(nil)

// Backup provides the term UI for a backup.
type BackupInfo struct {
	*tview.Box

	backup *agent.Backup

	flex       *tview.Flex
	startedAt  *tview.TextView
	finishedAt *tview.TextView
	took       *tview.TextView
	status     *tview.TextView
}

func (b *BackupInfo) Update () {
	b.startedAt.SetText("Started At: " + b.backup.StartedAt().Format(time.Kitchen))
	if !b.backup.FinishedAt().IsZero() {
		b.finishedAt.SetText("Finished At: " + b.backup.FinishedAt().Format(time.Kitchen))

		took := b.backup.FinishedAt().Sub(b.backup.StartedAt())
		b.took.SetText(fmt.Sprintf("Took: %.2fh %.0fm %.0fs", took.Hours(), took.Minutes(), took.Seconds()))
	} else {
		taken := time.Now().Sub(b.backup.StartedAt())
		b.took.SetText(fmt.Sprintf("Taken: %.2fh %.0fm %.0fs", taken.Hours(), taken.Minutes(), taken.Seconds()))
	}

	b.status.SetText("Status: " + string(b.backup.Status()))
}

// Draw implements tview.Primitive
func (b *BackupInfo) Draw(screen tcell.Screen) {
	b.Box.DrawForSubclass(screen, b)

	x, y, w, h := b.GetInnerRect()
	b.flex.SetRect(x, y, w, h)
	b.flex.Draw(screen)
}

// InputHandler implements tview.Primitive
func (b *BackupInfo) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return b.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		if handler := b.flex.InputHandler(); handler != nil {
			handler(event, setFocus)
			return
		}
	})
}
