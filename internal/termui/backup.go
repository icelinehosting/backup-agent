package termui

import (
	"strconv"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"iceline.host/backup-agent/internal/agent"
)

func NewBackupUI(backup *agent.Backup) *Backup {
	ObjectTable := tview.NewTable()
	ObjectTable.SetFixed(1, 3)
	ObjectTable.SetBorder(false)
	ObjectTable.SetBorders(false)
	ObjectTable.SetSelectable(true, false)

	object := NewObjectUI()

	ObjectTable.SetCell(0, 0,
		tview.NewTableCell("KEY").
			SetTextColor(tcell.ColorWhite).
			SetAlign(tview.AlignLeft).
			SetMaxWidth(1).
			SetExpansion(2).
			SetSelectable(false))
	ObjectTable.SetCell(0, 1,
		tview.NewTableCell("PATH").
			SetTextColor(tcell.ColorWhite).
			SetAlign(tview.AlignLeft).
			SetMaxWidth(1).
			SetExpansion(3).
			SetSelectable(false))
	ObjectTable.SetCell(0, 2,
		tview.NewTableCell("FILES").
			SetTextColor(tcell.ColorWhite).
			SetAlign(tview.AlignLeft).
			SetMaxWidth(1).
			SetExpansion(1).
			SetSelectable(false))
	ObjectTable.SetCell(0, 3,
		tview.NewTableCell("STATUS").
			SetTextColor(tcell.ColorWhite).
			SetAlign(tview.AlignLeft).
			SetMaxWidth(1).
			SetExpansion(1).
			SetSelectable(false))
	ObjectTable.SetSelectionChangedFunc(func(row, column int) {
		if idx := row - 1; idx >= 0 {
			object.SetObject(backup.Objects()[row-1])
		}
	})

	backupInfo := NewBackupInfoUI(backup)

	controlHints := tview.NewFlex().SetDirection(tview.FlexColumn).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewTextView().SetText("esc Cancel").SetTextColor(tcell.ColorGreen), 0, 1, false).
			AddItem(tview.NewTextView().SetText("^c Kill").SetTextColor(tcell.ColorGreen), 0, 1, false),
			0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(tview.NewTextView().SetText("up Select Previous Object").SetTextColor(tcell.ColorGreen), 0, 1, false).
			AddItem(tview.NewTextView().SetText("down Select Next Object").SetTextColor(tcell.ColorGreen), 0, 1, false),
			0, 1, false)
	controlHints.SetBorderPadding(0, 1, 2, 2)

	grid := tview.NewGrid().
		SetRows(2, -3, 4).
		SetColumns(-1, 30).
		SetBorders(true).
		AddItem(backupInfo, 0, 0, 1, 2, 0, 0, false) /* .
		AddItem(controlHints, 2, 0, 1, 2, 0, 0, false) */
	grid.SetBorderColor(tcell.ColorDarkGray)
	grid.SetBordersColor(tcell.ColorDarkGray)

	grid.AddItem(ObjectTable, 1, 0, 1, 2, 0, 0, false)
	grid.AddItem(object, 2, 0, 1, 2, 0, 0, false)

	flex := tview.NewFlex().SetDirection(tview.FlexRow)
	flex.AddItem(grid,
		0, 4, false)

	flex.AddItem(controlHints, 3, 1, false)

	return &Backup{
		Box: tview.NewBox(),

		backup: backup,

		ObjectTable: ObjectTable,
		flex:        flex,
		object:      object,
		info:        backupInfo,
		grid:        grid,
	}
}

var _ tview.Primitive = (*Backup)(nil)

// Backup provides the term UI for a backup.
type Backup struct {
	*tview.Box

	backup *agent.Backup

	ObjectTable *tview.Table
	flex        *tview.Flex
	object      *Object
	info        *BackupInfo
	grid        *tview.Grid
}

func (b *Backup) Update() {
	for row, obj := range b.backup.Objects() {
		prefix := " "
		if obj.Status == agent.BackupObjectUploaded {
			prefix = "✓ "
		}

		files := "-"
		if n := obj.Files.Load(); n > 0 {
			files = strconv.Itoa(int(obj.Files.Load()))
		}

		// b.ObjectTable.GetSelection()
		cols := []string{
			prefix + obj.Key,
			obj.Path,
			files,
			string(obj.Status),
		}
		for col, val := range cols {
			b.ObjectTable.SetCell(row+1, col,
				tview.NewTableCell(val).
					SetTextColor(tcell.ColorGray))
		}
	}

	b.info.Update()
	b.object.Update()
}

// Draw implements tview.Primitive
func (b *Backup) Draw(screen tcell.Screen) {
	b.Box.DrawForSubclass(screen, b)

	width, height := screen.Size()
	b.flex.SetRect(0, 0, width, height)

	b.flex.Draw(screen)
}

// InputHandler implements tview.Primitive
func (b *Backup) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return b.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		if handler := b.ObjectTable.InputHandler(); handler != nil {
			handler(event, setFocus)
			return
		}
	})
}
