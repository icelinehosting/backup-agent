package termui

import (
	"fmt"

	"github.com/gdamore/tcell/v2"
	"github.com/rivo/tview"

	"iceline.host/backup-agent/internal/agent"
)

func NewObjectUI () *Object {
	return &Object{
		Box: tview.NewBox(),
	}
}

var _ tview.Primitive = (*Object)(nil)

// Object provides the term UI for an object.
type Object struct {
	*tview.Box

	object *agent.BackupObject

	frame                *tview.Frame
	lastWalked           *tview.TextView
	currentlyCompressing *tview.TextView
	uploadProgress       *tview.TextView
	uploadProgressBytes  *tview.TextView
	objectError          *tview.TextView
}

func (o *Object) Object() *agent.BackupObject {
	return o.object
}

// SetObject sets the object to display
func (o *Object) SetObject(object *agent.BackupObject) {
	o.object = object

	lastWalked := tview.NewTextView().SetText("Loading...").SetTextColor(tcell.ColorGray)
	currentlyCompressing := tview.NewTextView().SetText("Loading...").SetTextColor(tcell.ColorGray)
	uploadProgress := tview.NewTextView().SetText("Loading...").SetTextColor(tcell.ColorGray)
	uploadProgressBytes := tview.NewTextView().SetText("Loading...").SetTextColor(tcell.ColorGray)
	objectError := tview.NewTextView().SetText("-").SetTextColor(tcell.ColorGray)
	frame := tview.NewFrame(tview.NewFlex().SetDirection(tview.FlexColumn).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(lastWalked, 0, 1, false).
			AddItem(currentlyCompressing, 0, 1, false).
			AddItem(objectError, 0, 2, false),
			0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(uploadProgress, 0, 1, false).
			AddItem(uploadProgressBytes, 0, 1, false),
		0, 1, false))
	frame.SetBorders(0, 0, 0, 0, 1, 1)
	if o.object != nil {
		frame.AddText("Object Info", true, tview.AlignLeft, tcell.ColorWhite)
		frame.AddText(object.Key, true, tview.AlignRight, tcell.ColorLightGray)
	} else {
		frame.AddText("Object Info", true, tview.AlignLeft, tcell.ColorWhite)
		frame.AddText("None Selected", true, tview.AlignRight, tcell.ColorLightGray)
	}

	o.frame = frame

	o.lastWalked = lastWalked
	o.currentlyCompressing = currentlyCompressing
	o.uploadProgress = uploadProgress
	o.uploadProgressBytes = uploadProgressBytes
	o.objectError = objectError
}

func (o *Object) Update () {
	if o.object != nil {
		o.lastWalked.SetText("Last read: " + o.object.LastWalked.Load())
		o.currentlyCompressing.SetText("Compressing: " + o.object.CurrentlyCompressing.Load())

		uploaded := o.object.BytesUploaded.Load()
		size := o.object.Size.Load()
		status := o.object.UploadState.Load()

		if uploaded > 0 && size > 0 {
			o.uploadProgress.SetText(fmt.Sprintf("Upload Progress: %.2f%% (%d/%d)",
				(float64(uploaded)/float64(size))*100, uploaded, size))
		} else {
			o.uploadProgress.SetText("Upload not started")
			o.uploadProgressBytes.SetText("")
		}

		o.uploadProgressBytes.SetText(
			fmt.Sprintf("Uploader Status: %s", status))
		o.objectError.SetText(o.object.Error.Load())
	}
}

// Draw implements tview.Primitive
func (o *Object) Draw(screen tcell.Screen) {
	o.Box.DrawForSubclass(screen, o)

	if o.frame != nil {
		x, y, w, h := o.GetRect()
		o.frame.SetRect(x, y, w, h)
		o.frame.Draw(screen)
	}
}

// InputHandler implements tview.Primitive
func (o *Object) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return o.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {

	})
}
